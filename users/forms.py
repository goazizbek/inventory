from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser, Profile



class CustomUserCreationForm(UserCreationForm):
    # company_name = forms.CharField(max_length=60)
    # phone = forms.CharField(max_length=60)
    #
    # def signup(self, request, user):
    #     user.company_name = self.cleaned_data['company_name']
    #     user.phone = self.cleaned_data['phone']
    #     user.save()
    #     return user

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')



class ProfileForm(forms.ModelForm):


    class Meta:
        model = Profile
        fields = ['company_name', 'phone_number', 'city', 'address']


