from django.db import models
from django.contrib.auth.models import AbstractUser, User
from django.urls import reverse

from common.models import TimeModel


class CustomUser(AbstractUser):
    # company_name = models.CharField(max_length=60, null=True)
    # phone = models.CharField(max_length=13, null=True)

    def __str__(self):
        return self.email


class Profile(TimeModel):
    user = models.OneToOneField('users.CustomUser', on_delete=models.CASCADE)
    company_name = models.CharField(max_length=100, null=True)
    phone_number = models.CharField(max_length=20, null=True)
    city = models.CharField(max_length=15, null=True)
    address = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.user.email


