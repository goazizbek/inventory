from django.db import models

# Create your models here.


class TimeModel(models.Model):

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:

        abstract = True
        ordering = ['-created_at', 'updated_at']