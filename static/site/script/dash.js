$(document).ready(function () {

    // $('.active_link').addClass("active");
    $('.active_link').click(function () {
        $('.active_link').removeClass("active");
        $(this).addClass("active");
    });

    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });

    $('select').niceSelect();
    $("#flip").click(function () {
        $("#panel-mahsulot").slideToggle("slow");
        $(".row2 .toogle-slide").toggleClass('rotate');
    });
    $('#comp-name').click(function () {
        $(this).addClass('toggle-color');
        $('#not').removeClass('toggle-color');
        $('.info-sec').css({
            display: 'block'
        });
        $('.info-sec .for-comp').addClass('toggle-display');
        $('.info-sec .for-not').removeClass('toggle-display');
    })
    $('#not').click(function () {
        $(this).addClass('toggle-color');
        $('#comp-name').removeClass('toggle-color');
        $('.info-sec').css({
            display: 'block'
        });
        $('.info-sec .for-comp').removeClass('toggle-display');
        $('.info-sec .for-not').addClass('toggle-display');
    });
    $('.info-sec .close-menu i').click(function () {
        $('.info-sec').css({
            display: 'none'
        });
        $('#not').removeClass('toggle-color');
        $('#comp-name').removeClass('toggle-color');
    });
    toolTiper();

    function toolTiper(effect) {
        $('.tooltiper').each(function () {
            var eLcontent = $(this).attr('data-tooltip'),
                eLtop = $(this).position().top,
                eLleft = $(this).position().left;
            $(this).append('<span class="tooltip">' + eLcontent + '</span>');
            var eLtw = $(this).find('.tooltip').width(),
                eLth = $(this).find('.tooltip').height();
            $(this).find('.tooltip').css({
                "top": (0 - eLth - 20) + 'px',
                "left": '5px'
            });
        });
    };
});

var checkboxes = document.querySelectorAll("tr input");

for (var i = 0, l = checkboxes.length; i < l; i++) {
    checkboxes[i].onclick = function (e) {
        e.stopPropagation();
    }
}

function active() {
    if (document.getElementById('sales_info').checked) {
        document.getElementById('id_selling_price').disabled = true;
    } else {
        document.getElementById('id_selling_price').disabled = false;
    }
}

function myFunc() {
    if (document.getElementById('purchase_info').checked) {
        document.getElementById('id_purchase_price').disabled = true;
    } else {
        document.getElementById('id_purchase_price').disabled = false;
    }
}



