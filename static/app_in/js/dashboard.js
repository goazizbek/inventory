$(document).ready(function () {

    $('#dtBasicExample').DataTable({
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": 0
                }
            ]
        });
        $('.dataTables_length').addClass('bs-select');0


    // $('.active_link').addClass("active");
    $('.active_link').click(function () {
        $('.active_link').removeClass("active");
        $(this).addClass("active");
    });

    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });

    $('#comp-name').click(function () {
        $(this).addClass('toggle-color');
        $('#not').removeClass('toggle-color');
        $('.info-sec').css({
            display: 'block'
        });
        $('.info-sec .for-comp').addClass('toggle-display');
        $('.info-sec .for-not').removeClass('toggle-display');
    })
    $('#not').click(function () {
        $(this).addClass('toggle-color');
        $('#comp-name').removeClass('toggle-color');
        $('.info-sec').css({
            display: 'block'
        });
        $('.info-sec .for-comp').removeClass('toggle-display');
        $('.info-sec .for-not').addClass('toggle-display');
    });
    $('.info-sec .close-menu i').click(function () {
        $('.info-sec').css({
            display: 'none'
        });
        $('#not').removeClass('toggle-color');
        $('#comp-name').removeClass('toggle-color');
    });
    toolTiper();

    function toolTiper(effect) {
        $('.tooltiper').each(function () {
            var eLcontent = $(this).attr('data-tooltip'),
                eLtop = $(this).position().top,
                eLleft = $(this).position().left;
            $(this).append('<span class="tooltip">' + eLcontent + '</span>');
            var eLtw = $(this).find('.tooltip').width(),
                eLth = $(this).find('.tooltip').height();
            $(this).find('.tooltip').css({
                "top": (0 - eLth - 20) + 'px',
                "left": '5px'
            });
        });
    };
});

var checkboxes = document.querySelectorAll("tr input");

for (var i = 0, l = checkboxes.length; i < l; i++) {
    checkboxes[i].onclick = function (e) {
        e.stopPropagation();
    }
}

function active() {
    if (document.getElementById('sales_info').checked) {
        document.getElementById('id_selling_price').disabled = true;
    } else {
        document.getElementById('id_selling_price').disabled = false;
    }
}

function myFunc() {
    if (document.getElementById('purchase_info').checked) {
        document.getElementById('id_purchase_price').disabled = true;
    } else {
        document.getElementById('id_purchase_price').disabled = false;
    }
}

$(function () {
    var duration = 4000; // 4 seconds
    setTimeout(function () { $('.alert').hide(); }, duration);
});

// Count checked checkbox
$('#general').hide();
var fnUpdateCount = function () {
        var generallen = $("input[name='wpmm[]']:checked").length;
        if (generallen > 0) {
            $("#counter").text(generallen);
            $('#general').show();
            $('#item_info').hide();
        } else {
            $("#counter").text(' ');
            $('#item_info').show();
            $('#general').hide();
        }
    };

    $("input:checkbox").on("change", function () {
        fnUpdateCount();
    });

    $('.select_all').change(function () {
        var checkthis = $(this);
        var checkboxes = $("input:checkbox");

        if (checkthis.is(':checked')) {
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        }
        fnUpdateCount();
    });


    // $('#general').hide();
    // $(function () {
    //     $('body').on('change', '#check', function (e) {
    //         e.preventDefault();
    //         if ($(this).prop('checked')) {
    //             $('#item_info').hide();
    //             $('#general').show();
    //         } else {
    //             $('#item_info').show();
    //             $('#general').hide();
    //         }
    //     });
    // });
    //
    // $('#general').hide();
    // $(function () {
    //     $('.select_all').on('change', function () {
    //         if ($('.select_all').prop('checked')) {
    //             $('#item_info').hide();
    //             $('#general').show();
    //         } else {
    //             $('#item_info').show();
    //             $('#general').hide();
    //         }
    //     });
    // });

