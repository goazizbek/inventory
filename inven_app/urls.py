from django.urls import path, re_path
from .views import *






urlpatterns = [
    path('', index, name='home'),
    path('contacts', ContactListView.as_view(), name='contacts'),
    path('contacts/new', ContactCreateView.as_view(), name='contact-create'),
    path('contacts/<int:pk>', ContactDetailView.as_view(), name='contact-detail'),
    path('contacts/update/<int:pk>', ContactUpdateView.as_view(), name='contact-update'),
    path('contacts/export/xls', export_contacts_xls, name='export-contact-excel'),
    path('contacts/import', contact_upload, name='contact-upload'),
    path('contacts/delete', delete_contact, name='contact-delete'),
    path('contacts/delete/<id>', contact_delete, name='contactid-delete'),

    path('items', ItemListView.as_view(), name='items'),
    path('items/new', ItemCreateView.as_view(), name='item-create'),
    path('items/<int:pk>', ItemDetailView.as_view(), name='item-detail'),
    path('items/export/xls', export_items_xls, name='export-item-excel'),
    path('items/import', item_upload, name='items-upload'),
    path('items/<int:pk>/update', ItemUpdate.as_view(), name='item-update'),
    path('items/delete', delete_items, name='item-delete'),
    path('items/delete/<id>', item_delete, name='items-delete'),

    path('sales_order', sales_order, name='sales-order'),
    path('purchase_order', purchase_order, name='purchase-order'),
    path('reports', reports, name='reports'),
]
