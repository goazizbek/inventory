# Generated by Django 2.2 on 2019-05-14 08:37

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('inven_app', '0005_auto_20190514_1007'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='item',
            options={},
        ),
        migrations.AlterField(
            model_name='item',
            name='sku',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterUniqueTogether(
            name='item',
            unique_together={('sku', 'author')},
        ),
    ]
