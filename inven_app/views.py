from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
import json
from django.http import Http404, HttpResponse
from django.contrib import messages
import xlwt
import csv, io
from .models import ClientContact, Item


def index(request):
    return render(request, 'site/index.html')


class ContactListView(LoginRequiredMixin, ListView):
    model = ClientContact
    template_name = 'site/contacts.html'
    context_object_name = 'contacts'
    ordering = ['-created_at', '-updated_at']

    def get_queryset(self):
        return super(ContactListView, self).get_queryset().filter(author=self.request.user)


class ContactDetailView(LoginRequiredMixin, DetailView):
    model = ClientContact
    template_name = 'site/clientcontact_detail.html'

    def get_queryset(self):
        return super(ContactDetailView, self).get_queryset().filter(author=self.request.user)

    def test_func(self):
        contact = self.get_object()
        if self.request.user == contact.author:
            return True
        return False


class ContactCreateView(LoginRequiredMixin, CreateView):
    model = ClientContact
    template_name = 'site/clientcontact_form.html'
    fields = ['client_name', 'client_company_name', 'email', 'mobile_phone', 'work_phone']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        contact = self.get_object()
        if self.request.user == contact.author:
            return True
        return False


class ContactUpdateView(LoginRequiredMixin, UpdateView):
    model = ClientContact
    template_name = 'site/contact_update.html'
    fields = ['client_name', 'client_company_name', 'email', 'mobile_phone', 'work_phone']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        contact = self.get_object()
        if self.request.user == contact.author:
            return True
        return False


@login_required
def export_contacts_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="contacts.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Contacts')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Client Name', 'Company Name', 'Email', 'Work Phone']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = ClientContact.objects.filter(author=request.user).values_list('client_name', 'client_company_name', 'email',
                                                                         'work_phone')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)

    return response


@login_required
def contact_upload(request):
    template = 'site/clientcontact_upload.html'

    if request.method == 'GET':
        return render(request, template)

    csv_file = request.FILES['file']

    if not csv_file.name.endswith('.csv'):
        messages.warning(request, 'This is not a csv file')
        return redirect('contact-upload')

    data_set = csv_file.read().decode('utf-8')
    io_string = io.StringIO(data_set)
    next(io_string)

    for column in csv.reader(io_string, delimiter=',', quotechar='|'):
        _, created = ClientContact.objects.filter().update_or_create(
            client_name=column[0],
            client_company_name=column[1],
            email=column[2],
            work_phone=column[3],
            author=request.user
        )
    context = {}
    messages.success(request, 'You have successfully added new contacts.')

    return redirect('contacts')


# @login_required
# def contact_delete(request, id):
#     contact = get_object_or_404(ClientContact, id=id)
#     if request.user != contact.author:
#         raise Http404
#     contact.selected = True
#     contact.delete()
#     return redirect('contacts')

@login_required
def delete_contact(request):
    if request.is_ajax():
        # contact = ClientContact.objects.get(author=request.user)
        selected_contacts = request.POST['contact_id']
        selected_contacts = json.loads(selected_contacts)

        for i, contact in enumerate(selected_contacts):
            if contact != '':
                ClientContact.objects.filter(id__in=selected_contacts).delete()
        return redirect('contacts')

@login_required
def contact_delete(request, id):
    contact = get_object_or_404(ClientContact, id=id)
    if request.user != contact.author:
        raise Http404
    contact.delete()
    messages.success(request, f'{contact}\'s information has been deleted successfully')
    return redirect('contacts')


class ItemListView(LoginRequiredMixin, ListView):
    model = Item
    template_name = 'item/items.html'
    context_object_name = 'items'
    ordering = ['-created_at', '-updated_at']

    def get_queryset(self):
        return super(ItemListView, self).get_queryset().filter(author=self.request.user)

from django.db.models import F, Sum, FloatField

class ItemCountView(LoginRequiredMixin, ListView):
    model = Item
    template_name = 'dashboard/dash.html'
    context_object_name = 'items'

    def get_queryset(self):
        return Item.objects.filter(author=self.request.user).count()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_profit'] = Item.objects.filter(author=self.request.user).aggregate(total=Sum(F('quantity') * F('selling_price'), output_field=FloatField() ))['total']
        context['items_on_sale'] = Item.objects.filter(author=self.request.user).order_by('?')[:3]
        context['lower_items'] = Item.objects.filter(author=self.request.user, quantity__lte=10).count()
        return context




class ItemDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Item
    template_name = 'item/item_detail.html'

    def test_func(self):
        item = self.get_object()
        if self.request.user == item.author:
            return True
        return False


class ItemCreateView(LoginRequiredMixin, CreateView):
    model = Item
    template_name = 'item/item_form.html'
    fields = ['item_name', 'sku', 'quantity', 'unit', 'manufacturer', 'brand', 'selling_price', 'purchase_price']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        item = self.get_object()
        if self.request.user == item.author:
            return True
        return False


class ItemUpdate(LoginRequiredMixin, UpdateView):
    model = Item
    template_name = 'item/item_update.html'
    fields = ['item_name', 'sku', 'quantity', 'unit', 'manufacturer', 'brand', 'selling_price', 'purchase_price']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        item = self.get_object()
        if self.request.user == item.author:
            return True
        return False


@login_required
def export_items_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="items.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Items')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Item Name', 'SKU', 'Quantity', 'Unit', 'Manufacturer', 'Brand', 'Selling Price', 'Purchase Price']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    font_style = xlwt.XFStyle()

    rows = Item.objects.filter(author=request.user).values_list('item_name', 'sku', 'quantity', 'unit', 'manufacturer',
                                                                'brand', 'selling_price', 'purchase_price')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)
    wb.save(response)

    return response

@login_required
def item_upload(request):
    template = 'item/items_upload.html'

    if request.method == 'GET':
        return render(request, template)

    csv_file = request.FILES['file']

    if not csv_file.name.endswith('.csv'):
        messages.warning(request, 'This is not a csv file. Items must be in CSV file')
        return redirect('items-upload')

    data_set = csv_file.read().decode('utf-8')
    io_string = io.StringIO(data_set)
    next(io_string)

    for column in csv.reader(io_string, delimiter=',', quotechar='|'):
        _, created = Item.objects.filter().update_or_create(
            item_name=column[0],
            sku=column[1],
            quantity=column[2],
            unit=column[3],
            manufacturer=column[4],
            brand=column[5],
            purchase_price=column[6],
            selling_price=column[7],
            author=request.user
        )
    context = {}
    messages.success(request, 'You have successfully added new items.')

    return redirect('items')

@login_required
def delete_items(request):
    if request.is_ajax():
        # contact = ClientContact.objects.get(author=request.user)
        selected_items = request.POST['item_id']
        selected_items = json.loads(selected_items)

        for i, item in enumerate(selected_items):
            if item != '':
                Item.objects.filter(id__in=selected_items).delete()
        return redirect('items')

@login_required
def item_delete(request, id):
    item = get_object_or_404(Item, id=id)
    if request.user != item.author:
        raise Http404
    item.delete()
    return redirect('items')


def sales_order(request):
    return render(request, 'item/sales_order.html')


def purchase_order(request):
    return render(request, 'item/purchase_order.html')


def reports(request):
    return render(request, 'item/reports.html')
