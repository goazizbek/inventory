from django.db import models
from common.models import TimeModel
from django.urls import reverse

class ClientContact(TimeModel):
    client_name = models.CharField(max_length=50)
    client_company_name = models.CharField(max_length=50)
    email = models.EmailField(null=True)
    mobile_phone = models.CharField(max_length=20, null=True)
    work_phone = models.CharField(max_length=20)
    author = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)

    def __str__(self):
        return self.client_company_name

    def get_absolute_url(self):
        return reverse('contact-detail', kwargs={'pk':self.pk})

SELECT_QUANTITY = (
    ('box', 'box'),
    ('cm', 'cm'),
    ('kg', 'kg'),
    ('m', 'm'),
    ('pcs', 'pcs'),
)

class Item(TimeModel):
    item_name = models.CharField(max_length=50)
    sku = models.CharField(max_length=20)
    quantity = models.IntegerField()
    unit = models.CharField(choices=SELECT_QUANTITY, max_length=5)
    manufacturer = models.CharField(max_length=50, null=True)
    brand = models.CharField(max_length=20, null=True)
    purchase_price = models.FloatField('purchase price (UZS)', max_length=255, blank=True, null=True)
    selling_price = models.FloatField('selling price (UZS)', max_length=255, blank=True, null=True)
    author = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('sku', 'author')

    def __str__(self):
        return self.item_name

    def get_absolute_url(self):
        return reverse('item-detail', kwargs={'pk':self.pk})

    @property
    def total_sum(self):
        return self.quantity * self.selling_price


