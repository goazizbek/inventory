from django.contrib import admin

from .models import ClientContact, Item

admin.site.register(ClientContact)
admin.site.register(Item)
